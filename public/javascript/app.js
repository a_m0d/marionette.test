define([
        'backbone',
        'marionette',
        ],
        function (Backbone, Marionette) {
            var app = new Marionette.Application();

            app.addRegions({
                header: '#header',
                footer: '#footer',
                main: '#main'
            });

            app.addInitializer(function () {
                Backbone.history.start();
                console.log('App up and running!');
            });

            app.vent.on('loading', function () {
                console.log('Loading ...');
            });

            app.vent.on('loaded', function () {
                console.log('Loaded');
            });

            return app;
        }
);
