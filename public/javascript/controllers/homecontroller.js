define(['marionette', 'app', 'collections/list', 'views/messagelistview'],
        function (Marionette, App, List, MessageListView) {
            return Marionette.Controller.extend({
                index: function () {
                    var list = new List();
                    list.fetch();
                    App.main.show(new MessageListView({
                        collection: list
                    }));
                }
            });
        }
      );
