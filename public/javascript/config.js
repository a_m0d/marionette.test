require.config({
    baseUrl: '/javascript',
    shim: {
        backbone: {
            exports: 'Backbone',
            deps: ['jquery', 'underscore']
        },
        underscore: {
            exports: '_'
        },
        marionette: {
            exports: 'Backbone.Marionette',
            deps: ['backbone']
        }
    },
    paths: {
        backbone: '/backbone/backbone',
        underscore: '/underscore/underscore',
        marionette: '/backbone.marionette/lib/backbone.marionette',
        jquery: '/jquery/jquery',
        text: '/requirejs-text/text'
    }
});

require([
        'app',
        'router',
        'controllers/homecontroller'
        ],
        function (App, Router, HomeController) {
            App.appRouter = new Router({
                controller: new HomeController()
            });

            App.start();
        }
);
