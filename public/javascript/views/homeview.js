define(['marionette'],
        function (Marionette) {
            return Marionette.ItemView.extend({
                template: '#welcome',
            ui: {
                clicker: '#clicker'
            },
            events: {
                'click #clicker': 'showMessage'
            },

            showMessage: function () {
                require(['app'], function (App) {
                    App.vent.trigger('loading');
                    require(['app', 'views/footer'],
                        function (App, Footer) {
                            App.footer.show(new Footer());
                            App.vent.trigger('loaded');
                        }
                        );
                });
            }
            });
        }
);
