define(['marionette', 'text!/templates/footer.html'],
        function (Marionette, FooterTemplate) {
            return Marionette.ItemView.extend({
                template: _.template(FooterTemplate)
            });
        }
      );
