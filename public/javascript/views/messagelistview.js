define(['marionette',
        'collections/list',
        'views/homeview'],
        function (Marionette, List, HomeView) {
            return Marionette.CollectionView.extend({
                itemView: HomeView,
                collection: List
            });
        }
);
