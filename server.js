var express = require('express'),
    path = require('path');
var app = express();

app.configure(function () {
    app.use(express.static(path.join(__dirname, 'public')));
    app.use(express.static(path.join(__dirname, 'bower_components')));
});

app.get('/list', function (req, resp) {
    var body = JSON.stringify([{name: 'Bob'}, {name: 'Sam'}]);
    resp.setHeader('Content-Type', 'application/json');
    resp.setHeader('Content-Length', body.length);

    resp.end(body);
});

app.listen(3000);
console.log('Up and running on :3000');
